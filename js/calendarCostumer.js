$(document).ready(function() {
    //OBTENESMOS LA HORA EN TIEMPO REAL
    var fHNow = moment(new Date());
    var today = moment(new Date()).format("HH");
    var Hmin = moment(new Date()).format("HH:mm");    
    var vToday = new Date().getDay();
    
    var userSession = "<?php echo $_SESSION['user'] ?>"; 
    
    
    console.log(userSession)

    //FUNCIONES PARA EL CALENDARIO     
    var calendar = $('#calendar').fullCalendar({     
            nowIndicator: true,
            now: fHNow,
            defaultDate: fHNow,
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            axisFormat: 'H:mm',
            timeFormat: {
                agenda: 'H:mm'
            },
            height: 620,
            events: './db/load.php',
            textColor: 'black',
            defaultView: 'agendaWeek',
            slotDuration: '00:15',
            buttonText: ':15 slots',
//        slotDuration: '00:15',
            navLinks: true, // can click day/week names to navigate views
            selectable: true,
            eventRender: function(eventObj, $el) {
                $el.popover({
                  title: eventObj.hI +" a "+ eventObj.hF + " ("+ eventObj.usuario+")" ,
                  content: "Delivery: \n"+eventObj.title+ " \n               Sold To: "+ eventObj.soldTo ,
                   //"                Estado: "+ eventObj.descEstado
                  trigger: 'hover',
                  placement: 'top',
                  container: 'body'
                });
              },
            //selectHelper: true,
            select: function(start, end, allDay) { 
                //VALIDA QUE SEA ENTRE SEMANA L-V
                if (vToday > 0 && vToday < 6 ){
                    if (today != 5 ){
                        //CUANDO SEA DISTINTO DE VIERNES
                        //VALIDACION PARA NO REGISTRAR DESPUES DE LAS 5 PM                        
                        if(today < 17 ){
                            $('#newEvent').modal('show');
                        }else {
                            //LANZA EL MODAL DE LAS HORAS
                            $('#warningHora').modal('show');
                        }
                    }else {
                        //SI ES VIERNES ENTONCES DEJA HACER REGISTROS HASTA LAS 7 PM
                        if(today < 19 ){
                            $('#newEvent').modal('show');
                        }else {
                            //SI ES VIERNES Y PASAN DE LAS 7 PM LANZA MODAL DE HORAS
                            $('#warningHora').modal('show');
                        }
                    }
                } else{
                    //SI SE INTENTA HACR REGISTRO ENTRE FIN DE SEMANA LANZA MODAL DE FIN DE SEMANA
                    $('#warningFinSemana').modal('show');
                }
            },
            //AQUI INICIA EL MODULO PARA EDICION DE EVENTOS
            eventClick: function(event) { 
                //alert(event.hF);
                
                if (vToday > 0 && vToday < 6 ){
                    if (today != 5 ){
                        //CUANDO SEA DISTINTO DE VIERNES
                        //VALIDACION PARA NO REGISTRAR DESPUES DE LAS 5 PM  
                        
                        if(today < 17 && event.hI > Hmin ){
                            //PASAMOS LOS VALORES DEL EVENTO SELECCIONADO
                            //DATOS DE EVENTO                            
                            $('#id').val(event.id); 
                            $('#fecha').val(moment(event.fecha).format('DD/MM/YYYY'));    
                            $('#inicio').val(event.hI); 
                            $('#fin').val(event.hF); 
                            
                            //DATOS DE ENVIO (SOLD TO, NOMBRE SOLDTO, DELIVERY, PALLETS, PZAS)
                            $('#sold').val(event.soldTo);
                            $('#nombreCliente').val(event.cliente);
                            $('#delivery').val(event.title);  
                            $('#carga').val(event.pallets);
                            $('#pzas').val(event.piezas);  
                               
                            //DATOS DE TRANSPORTE(PLACA, RUTA DOCK, CHOFER)
                            $('#placa').val(event.placas);
                            $('#ruta').val(event.ruta);
                            $('#dock').val(event.puerta);    
                            $('#chofer').val(event.chofer);
                            
                            //DATOS DE ESTADO                            
                            
                            $('#updateEvent').modal('show');
                                                       
                        }else {
                            //LANZA MODAL DE VISUALIZACION
                            $('#idCons').val(event.id); 
                            $('#fechaCons').val(moment(event.fecha).format('DD/MM/YYYY'));    
                            $('#startCons').val(event.hI); 
                            $('#endCons').val(event.hF); 
                            $('#estadoCons').val(event.descEstado);

                            //DATOS DE ENVIO (SOLD TO, NOMBRE SOLDTO, DELIVERY, PALLETS, PZAS)
                            $('#soldCons').val(event.soldTo);
                            $('#nombreClienteCons').val(event.cliente);
                            $('#deliveryCons').val(event.title);  
                            $('#cargaCons').val(event.pallets);
                            $('#pzasCons').val(event.piezas);  

                            //DATOS DE TRANSPORTE(PLACA, RUTA DOCK, CHOFER)
                            $('#placaCons').val(event.placas);
                            $('#rutaCons').val(event.ruta);
                            $('#dockCons').val(event.puerta);    
                            $('#choferCons').val(event.chofer);
                            //DATOS DE ESTADO
                            $('#selectEvent').modal('show');                            
                        }
                    }else {
                        //SI ES VIERNES ENTONCES DEJA HACER REGISTROS HASTA LAS 7 PM
                        if(today < 19 && event.hI > Hmin ){
                            $('#updateEvent').modal('show');
                        }else {
                            //SI ES VIERNES Y PASAN DE LAS 7 PM LANZA MODAL DE HORAS
                            //LANZA MODAL DE VISUALIZACION
                            $('#idCons').val(event.id); 
                            $('#fechaCons').val(moment(event.fecha).format('DD/MM/YYYY'));    
                            $('#startCons').val(event.hI); 
                            $('#endCons').val(event.hF); 
                            $('#estadoCons').val(event.descEstado);

                            //DATOS DE ENVIO (SOLD TO, NOMBRE SOLDTO, DELIVERY, PALLETS, PZAS)
                            $('#soldCons').val(event.soldTo);
                            $('#nombreClienteCons').val(event.cliente);
                            $('#deliveryCons').val(event.title);  
                            $('#cargaCons').val(event.pallets);
                            $('#pzasCons').val(event.piezas);  

                            //DATOS DE TRANSPORTE(PLACA, RUTA DOCK, CHOFER)
                            $('#placaCons').val(event.placas);
                            $('#rutaCons').val(event.ruta);
                            $('#dockCons').val(event.puerta);    
                            $('#choferCons').val(event.chofer);
                            //DATOS DE ESTADO
                            $('#selectEvent').modal('show');
                        }
                    }
                } else{
                    //SI SE INTENTA HACER REGISTRO ENTRE FIN DE SEMANA LANZA MODAL DE FIN DE SEMANA
                    //LANZA MODAL DE VISUALIZACION
                    $('#idCons').val(event.id); 
                    $('#fechaCons').val(moment(event.fecha).format('DD/MM/YYYY'));    
                    $('#startCons').val(event.hI); 
                    $('#endCons').val(event.hF); 
                    $('#estadoCons').val(event.descEstado);

                    //DATOS DE ENVIO (SOLD TO, NOMBRE SOLDTO, DELIVERY, PALLETS, PZAS)
                    $('#soldCons').val(event.soldTo);
                    $('#nombreClienteCons').val(event.cliente);
                    $('#deliveryCons').val(event.title);  
                    $('#cargaCons').val(event.pallets);
                    $('#pzasCons').val(event.piezas);  

                    //DATOS DE TRANSPORTE(PLACA, RUTA DOCK, CHOFER)
                    $('#placaCons').val(event.placas);
                    $('#rutaCons').val(event.ruta);
                    $('#dockCons').val(event.puerta);    
                    $('#choferCons').val(event.chofer);
                    //DATOS DE ESTADO
                    $('#selectEvent').modal('show');
                }
            },

        });
        
    //FUNCION PARA ACTUALIZAR LOS EVENTO CADA MINUTO
    var refreshId = setInterval(function() {
                    calendar.fullCalendar('refetchEvents');
                }, 60000);
    $.ajaxSetup({ cache: false });        
        
    //FUNCIONES PARA LOS MODALS HACER LOS QUERYS 
    //FUNCION PARA AGREGAR NUEVO REGISTRO A LA BASE DE DATOS
    $( "#nuevoEvento" ).submit(function( event ) {    
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "./db/iEvento.php",
                data: parametros,                
                success: function(datos){
                    $("#datos_ajax_event").html(datos);
                    calendar.fullCalendar('refetchEvents');
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });
    
    $( "#modificarEvento" ).submit(function( event ) {    
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "./db/uEvento.php",
                data: parametros,
                beforeSend: function(objeto){
                    $("#datos_ajax_register").html(parametros);
                },
                success: function(datos){
                    $("#datos_ajax").html(datos);
                    calendar.fullCalendar('refetchEvents');
                },
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });
    
    
     
    //FUNCION PARA CUANDO SE LOGUEA, SE HACEN LAS CONSULTAS NECESARIAS    
    $( "#logiin" ).submit(function( event ) {  //FUNCION CUANDO LE DAN EN LOGIN
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "./db/cLogin.php",
                data: parametros,
                beforeSend: function(objeto){
                    $("#datos_ajax_register").html(parametros);
                },
                success: function(response){
                    $("#datos_ajax").html(datos);
                    calendar.fullCalendar('refetchEvents');
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });
    
    $( "#exit" ).submit(function( event ) {  //FUNCION CUANDO LE DAN EN LOGIN
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "./extras/exit.php",
                data: parametros,
                beforeSend: function(objeto){
                    $("#datos_ajax_register").html(parametros);
                },
                success: function(response){
                    calendar.fullCalendar('refetchEvents');
                    location.href="../shipp.php";
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });
    
    
    //ACTUALIZA LOS DATOS EN LA BD SE MANDA EL QUERY UPDATE
    $( "#actualidarDatosUser" ).submit(function( event ) {    
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "./extras/uUserUser.php",
                data: parametros,
                success: function(datos){
                    $("#datos_ajax").html(datos);
                    load();
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });
});  