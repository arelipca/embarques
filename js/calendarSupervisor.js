$(document).ready(function() {
    //OBTENESMOS LA HORA EN TIEMPO REAL
    var fHNow = moment(new Date());
    var today = moment(new Date()).format("HH");
    var vToday = new Date().getDay();
    
    //FUNCIONES PARA EL CALENDARIO     
    var calendar = $('#calendar').fullCalendar({     
            nowIndicator: true,
            now: fHNow,
            defaultDate: fHNow,
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            axisFormat: 'H:mm',
            timeFormat: {
                agenda: 'H:mm'
            },
            height: 620,
            events: './db/load.php',
            textColor: 'black',
            defaultView: 'agendaWeek',
            slotDuration: '00:15',
            buttonText: ':15 slots',
//        slotDuration: '00:15',
            navLinks: true, // can click day/week names to navigate views
            selectable: true,
            eventRender: function(eventObj, $el) {
                $el.popover({
                  title: eventObj.hI +" a "+ eventObj.hF + " ("+ eventObj.usuario+")" ,
                  content: "Delivery: \n"+eventObj.title+ " \n               Sold To: "+ eventObj.soldTo ,
                   //"                Estado: "+ eventObj.descEstado
                  trigger: 'hover',
                  placement: 'top',
                  container: 'body'
                });
            },
            //selectHelper: true,
            select: function(start, end, allDay) {
                calendar.fullCalendar('refetchEvents');
                $('#newEvent').modal('show');
            },
            //AQUI INICIA EL MODULO PARA EDICION DE EVENTOS
            eventClick: function(event) { 
                //alert(event.usuarioAsig);
                $('#id').val(event.id); 
                $('#fecha').val(moment(event.fecha).format('DD/MM/YYYY'));    
                $('#inicio').val(event.hI); 
                $('#fin').val(event.hF); 
                $('#dUsuario').val(event.usuarioAsig);                

                //DATOS DE ENVIO (SOLD TO, NOMBRE SOLDTO, DELIVERY, PALLETS, PZAS)
                $('#sold').val(event.soldTo);
                $('#nombreCliente').val(event.cliente);
                $('#delivery').val(event.title);  
                $('#carga').val(event.pallets);
                $('#pzas').val(event.piezas);  

                //DATOS DE TRANSPORTE(PLACA, RUTA DOCK, CHOFER)
                $('#placa').val(event.placas);
                $('#ruta').val(event.ruta);
                $('#dock').val(event.puerta);    
                $('#chofer').val(event.chofer);

                //DATOS DE ESTADO                            

                $('#updateEvent').modal('show');
            },

        });
        
    //FUNCION PARA ACTUALIZAR LOS EVENTO CADA MINUTO
    var refreshId = setInterval(function() {
                    calendar.fullCalendar('refetchEvents');
                }, 60000);
    $.ajaxSetup({ cache: false });      
        
    //FUNCIONES PARA LOS MODALS HACER LOS QUERYS 
    //FUNCION PARA AGREGAR NUEVO REGISTRO A LA BASE DE DATOS
    $( "#nuevoEvento" ).submit(function( event ) {    
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "./db/iEvento.php",
                data: parametros,
                beforeSend: function(objeto){
                    $("#datos_ajax_register").html(parametros);
                },
                success: function(datos){
                    $("#datos_ajax_event").html(datos);
                    calendar.fullCalendar('refetchEvents');
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });
    
    $( "#modificarEvento" ).submit(function( event ) {    
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "./db/uEvento.php",
                data: parametros,
                beforeSend: function(objeto){
                    calendar.fullCalendar('refetchEvents');
                    $("#datos_ajax_register").html(parametros);
                },
                success: function(datos){
                    $("#datos_ajax").html(datos);
                    calendar.fullCalendar('refetchEvents');                    
                },
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });
    
    
     
    //FUNCION PARA CUANDO SE LOGUEA, SE HACEN LAS CONSULTAS NECESARIAS    
    $( "#logiin" ).submit(function( event ) {  //FUNCION CUANDO LE DAN EN LOGIN
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "./db/cLogin.php",
                data: parametros,
                beforeSend: function(objeto){
                    $("#datos_ajax_register").html(parametros);
                },
                success: function(response){
                    //location.href="../shipp.php";
                    //-location.reload(true);	
                    calendar.fullCalendar('refetchEvents');
                    //alert('Paso Login');
                    //load();
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });
    
    $( "#exit" ).submit(function( event ) {  //FUNCION CUANDO LE DAN EN LOGIN
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "./extras/exit.php",
                data: parametros,
                beforeSend: function(objeto){
                    $("#datos_ajax_register").html(parametros);
                },
                success: function(response){
                    //location.href="../shipp.php";
                    location.reload(true);	
                    //alert('Paso Login');
                    //load();
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });
    
});  