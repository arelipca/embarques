/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function() {
    //OBTENESMOS LA HORA EN TIEMPO REAL
    //var fHNow = '2018-06-21T09:25:00';
    var fHNow = moment(new Date());
    console.log(fHNow);
    var today = moment(new Date()).format("HH");
    var vToday = new Date().getDay();

    //FUNCIONES PARA EL CALENDARIO 
    var calendar = $('#calendar').fullCalendar({   
            nowIndicator: true,
            now: fHNow,
            defaultDate: fHNow,
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            axisFormat: 'H:mm',
            timeFormat: {
                agenda: 'H:mm'
            },
            height: 620,
            events: './db/load.php',
            defaultView: 'agendaWeek',
            slotDuration: '00:15',
            buttonText: ':15 slots',
            navLinks: true, // can click day/week names to navigate views
            selectable: false,
            eventRender: function(eventObj, $el) {
                $el.popover({
                  title: eventObj.hI +" a "+ eventObj.hF + " ("+ eventObj.usuario+")" ,
                  content: "Delivery: \n"+eventObj.title+ " \n               Sold To: "+ eventObj.soldTo ,
                   //"                Estado: "+ eventObj.descEstado
                  trigger: 'hover',
                  placement: 'center',
                  container: 'body'
                });
              },
            //AQUI INICIA EL MODULO PARA EDICION DE EVENTOS
            eventClick: function(event) { 
                //PASAMOS LOS VALORES DEL EVENTO SELECCIONADO
                //DATOS DE EVENTO
                $('#idCons').val(event.id); 
                $('#fechaCons').val(moment(event.fecha).format('DD/MM/YYYY'));    
                $('#inicioCons').val(event.hI); 
                $('#finCons').val(event.hF); 
                $('#estadoCons').val(event.descEstado);

                //DATOS DE ENVIO (SOLD TO, NOMBRE SOLDTO, DELIVERY, PALLETS, PZAS)
                $('#soldCons').val(event.soldTo);
                $('#nombreClienteCons').val(event.cliente);
                $('#deliveryCons').val(event.title);  
                $('#cargaCons').val(event.pallets);
                $('#pzasCons').val(event.piezas);  

                //DATOS DE TRANSPORTE(PLACA, RUTA DOCK, CHOFER)
                $('#placaCons').val(event.placas);
                $('#rutaCons').val(event.ruta);
                $('#dockCons').val(event.puerta);    
                $('#choferCons').val(event.chofer);
                
                //DATOS DE ESTADO
                $('#selectEvent').modal('show');
            },

        });
     
     
    //FUNCION PARA ACTUALIZAR LOS EVENTO CADA MINUTO
    var refreshId = setInterval(function() {
                    calendar.fullCalendar('refetchEvents');
                }, 60000);
    $.ajaxSetup({ cache: false });     
    
    //FUNCION PARA CUANDO SE LOGUEA, SE HACEN LAS CONSULTAS NECESARIAS    
    $( "#logiin" ).submit(function( event ) {  //FUNCION CUANDO LE DAN EN LOGIN
        var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "./db/cLogin.php",
                data: parametros,
                success: function(datos){
                    $("#datos_ajax").html(datos);
                }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });    
});  