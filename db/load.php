<?php
    $server = "SGLERSQL01\sqlexpress, 1433";  
    $database = "DB_LER_SHIPMON_SQL";  
    $conn = new PDO( "sqlsrv:server=$server ; Database = $database", "USR_SHIPMON_SQL", "7ET73jsQxB4hBBrX");  

    date_default_timezone_set("America/Mexico_City");
            //OBTENEMOS LA FECHA POR SEPARADO
    $fecha  = date("Y-m-d");
    $hora = date("H");
    $min = date("i");
    $hIni = $hora.$min;

    //ESTA VALIDACION ES LA QUE HACE QUE SE PINTE EL EVENTO DE ROJO CUANDO ESTA EN TIEMPO REAL
    if ($min == 0 || $min == 15 || $min == 30 || $min == 45){
        $stmt = $conn->query( "UPDATE registros SET color = '#DC1400' WHERE fecha = '$fecha' AND hIni = '$hIni' AND estado < '3'" ); 
    }
    
    
    $stmt = $conn->query( "SELECT * FROM registros WHERE tipoRegistro <> 1" );  
    $result = $stmt->fetchAll(PDO::FETCH_BOTH);  
    foreach($result as $row){
        $data[] = array(
            'id'=> $row["id"],
            'fecha'=> $row["fecha"],
            'start'=> $row["inicio"],
            'hIni'=> $row["hIni"],
            'hI'=> $row["ini"],
            'hF'=> $row["fn"],
            'end'=> $row["fin"],
            'title'=> $row["delivery"],
            'soldTo'=> $row["soldTo"],
            'cliente'=> $row["cliente"],
            'pallets'=> $row["pallets"],
            'piezas'=> $row["piezas"],
            'placas'=> $row["placas"],
            'ruta'=> $row["ruta"],
            'chofer'=> $row["chofer"],
            'usuario'=> $row["usuario"],
            'usuarioAsig'=>$row["usuarioDesignado"],
            'puerta'=> $row["puerta"],
            'estado'=> $row["estado"],
            'descEstado'=> $row["descEstado"],
            'color'=> $row["color"]
        );
    }

echo json_encode($data);

?>
