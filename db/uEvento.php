<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    session_start();
    //ESTE MODULO ES SOLO PARA FECHA Y HORA 
    $id = $_POST['id'];
    
    $date = explode("/", $picker);
    
    $m = $date[0];    
    $d = $date[1];  
    $y = $date[2];
    
    $fecha = $y.'-'.$m.'-'.$d;
    
    $hI = $_POST['start'];
    $hF = $_POST['end'];
    
    //AQUI CORTAMOS PARA PODER MANDAR SOLO EL VALOR DE HORA INICIO Y FIN PARA PODER COMPARAR POSTERIORMENTE
    $start = explode(":", $hI);
    $end = explode(":", $hF);

    $hInicio = $start[0].$start[1];      
    $hFin = $end[0].$end[1]; 
    
    $registroInicio = $fecha.' '.$hI;
    $registroFin = $fecha.' '.$hF;
    
    //HACEMOS EL POST DE LA INFORMACION DEL MODAL
    $soldTo = $_POST["sold"];
    $cliente = $_POST["nombreCliente"];
    $delivery = $_POST["delivery"];
    $pallets = $_POST["pallets"];
    $piezas = $_POST["cantidad"];
    $placas = $_POST["placa"];
    $ruta = $_POST["ruta"];
    $chofer = $_POST["chofer"];
    //USUARIO SE OBTIENE DEL INICIO DE SESION QUE SE REGISTRO
    $usuario = $_SESSION["user"];
    $usuarioAsignado = isset($_POST["dUsuario"]) ? $_POST["dUsuario"] : "";
    $puerta = $_POST['dock'];
    
    
    $tipoRegistro = 0;
    
    
    $serverName = "SGLERSQL01\sqlexpress, 1433"; 
    $options = array("Database"=>"DB_LER_SHIPMON_SQL", "UID"=>"USR_SHIPMON_SQL ", "PWD"=>"7ET73jsQxB4hBBrX");
    $conn = sqlsrv_connect($serverName, $options);
    
   
    if( $conn ) {       
        //APARTADO DE INSERT
        if ($_SESSION["tipo"] == 1){
            $estado = $_POST['estado'];
            $numFactura = $_POST["factura"];
            
            switch ($estado){
                case 1:
                    $descEstado = "Shipment Creado";
                    $color = "#828a8e";
                    break;
                case 2:
                    $descEstado = "Loading Start";
                    $color = "#E3A01B";
                    break;
                case 3:
                    $descEstado = "Shipment Start";
                    $color = "#E3A01B";
                    break;
                case 4:
                    $descEstado = "Shipment Invoiced";
                    $color = "#E3A01B";
                    break;
                case 5:
                    $descEstado = "Shipment Complete";
                    $color = "#2ECC71";
                    break;
                case 6:
                    $tipoRegistro = 1;
                    $descEstado = "Delay";
                    $color = "#DC1400";
                    break;
            }
            $query = "UPDATE registros SET tipoRegistro = '$tipoRegistro', estado = '$estado', "
                . "descEstado = '$descEstado', color = '$color', pallets = '$pallets', piezas = '$piezas', placas = '$placas', ruta = '$ruta', chofer = '$chofer', puerta = '$puerta', numFactura = '$numFactura' WHERE id = '$id' AND soldTo = '$soldTo' AND delivery = '$delivery' ";

        } else {
             //echo " color = '$color', pallets = '$pallets', piezas = '$piezas', placas = '$placas', ruta = '$ruta', chofer = '$chofer', puerta = '$puerta' , id = '$id' AND soldTo = '$soldTo' AND delivery = '$delivery'";
            $query = "UPDATE registros SET pallets = '$pallets', piezas = '$piezas', placas = '$placas', ruta = '$ruta', chofer = '$chofer', puerta = '$puerta' WHERE id = '$id' AND soldTo = '$soldTo' AND delivery = '$delivery' ";
        }
        $result = sqlsrv_query($conn,$query);
        //}        
        sqlsrv_close($conn);        
    } else{
        $errors []= "Connection could not be established.<br />";
        die( print_r( sqlsrv_errors(), true));        
    }
    
    
    
    if (isset($errors)){
    ?>
        <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong><?php foreach ($errors as $error) {
            echo $error;
        } ?> </strong>     
        </div>
    <?php } else {
        ?>
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Registro guardado Correctamente</strong>     
        </div>
        <?php
        echo "<script>
            $('#updateEvent').modal('hide');            
            </script>";
    } ?>