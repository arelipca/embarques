<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    session_start();
    $picker = $_POST['datepicker'];
    
    $date = explode("/", $picker);
    
    $m = $date[0];    
    $d = $date[1];  
    $y = $date[2];
    
    $fecha = $y.'-'.$m.'-'.$d;    
    
    $hI = $_POST['start'];
    $hF = $_POST['end'];
    //AQUI CORTAMOS PARA PODER MANDAR SOLO EL VALOR DE HORA INICIO Y FIN PARA PODER COMPARAR POSTERIORMENTE
    $start = explode(":", $hI);
    $end = explode(":", $hF);

    $hInicio = $start[0].$start[1];      
    $hFin = $end[0].$end[1]; 
    
    $tTotal = $hFin-$hInicio;
    
    $registroInicio = $fecha.' '.$hI;
    $registroFin = $fecha.' '.$hF;

    $soldTo = $_POST["cliente"];
    $cliente = $_POST["nameCliente"];
    $delivery = $_POST["delivery"];
    $pallets = $_POST["pallets"];
    $piezas = $_POST["cantidad"];
    $placas = $_POST["placa"];
    $ruta = $_POST["ruta"];
    $chofer = $_POST["chofer"];
    //USUARIO SE OBTIENE DEL INICIO DE SESION QUE SE REGISTRO
    $usuario = $_SESSION["user"]; 
    $usuarioAsignado = isset($_POST["userAsig"]) ? $_POST["userAsig"] : "";
    $puerta = $_POST['dock'];
    
    $estado = 1;
    $tipoRegistro = 0;
    
    $descEstado = "Shipment Creado";
    $color = "#828a8e";   
    
    $cont = 0 ;
    
    if ($hInicio > $hFin ){
        $errors []= "La hora inicial: ".$hI." no puede ser mayor a la final: ".$hF;
    } else if ($hInicio != $hFin && $tTotal <= 200 ) {
        $serverName = "SGLERSQL01\sqlexpress, 1433"; 
        $options = array("Database"=>"DB_LER_SHIPMON_SQL", "UID"=>"USR_SHIPMON_SQL ", "PWD"=>"7ET73jsQxB4hBBrX");
        $conn = sqlsrv_connect($serverName, $options);

        if( $conn ) {               
            //CONSULTA PARA EL TRASLAPE DE HORAS EN EL MISMO DOCK
            $query = "SELECT *FROM registros WHERE fecha = '$fecha' AND puerta = '$puerta' AND tipoRegistro <> 1 AND hIni = '$hInicio' AND hFin = '$hFin' OR hIni < '$hInicio' AND hFin > '$hFin' OR hIni < '$hInicio' AND hFin > '$hFin' OR  hIni < '$hInicio' AND hFin > '$hInicio' AND hFin < '$hFin'";
            $result = sqlsrv_query($conn,$query);
            //echo 'fecha = ',$fecha,' hIni: ',$hInicio,' hFin: ',$hFin,'puerta = ',$puerta;

            while($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)){
                $cont = $cont+1; //contador de registros que trae despues de la consulta
            }
            if ($cont < 1 ){//AQUI VALIDAMOS QUE NO SE TRASLAPE LA HORA
                //APARTADO DE INSERT
                $query = "INSERT INTO registros(fecha, hIni, hFin, inicio, fin, soldTo, cliente, delivery, pallets, piezas, placas, ruta, chofer, usuario, usuarioDesignado, puerta , estado, color, ini, fn, descEstado, tipoRegistro)"
                . " VALUES ('$fecha','$hInicio','$hFin','$registroInicio','$registroFin','$soldTo','$cliente','$delivery','$pallets','$piezas','$placas','$ruta','$chofer','$usuario','$usuarioAsignado','$puerta','$estado','$color','$hI','$hF','$descEstado','$tipoRegistro')";

                $result = sqlsrv_query($conn,$query);
            } else {
                $errors []= "El registro no se pudo realizar por Traslape de Horas";
            }       
            sqlsrv_close($conn);        
        } else{
            echo "Connection could not be established.<br />";
            $errors []= "Revisar Conexion al Servidor.";
            die( print_r( sqlsrv_errors(), true));        
        }

    } else if ($hInicio == $hFin ){
        $errors []= "El registro no se pudo relizar: el registro debe durar minimo 15 min";
    } else if ($tTotal > 200){
        $errors []= "El registro no se pudo relizar: la duracion excede de 2 hrs";
    }  
    
    if (isset($errors)){
    ?>
        <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong><?php foreach ($errors as $error) {
            echo $error;
        } ?> </strong>     
        </div>
    <?php } else {
        ?>
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Registro guardado Correctamente</strong>     
        </div>
        <?php
        echo "<script>
            $('#newEvent').modal('hide');            
            </script>";
    } ?>