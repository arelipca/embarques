<!DOCTYPE html>
<html>
    <head>
        <title>SHIPPING MONITOR</title>
        <link rel="stylesheet" href="css/fullCalendar.css"/>
        <link rel="stylesheet" href="css/fullcalendar.min.css"/>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
        <!--<link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.6.1/fullcalendar.min.css" type="text/css" rel="stylesheet" />-->
        <script src="https://code.jquery.com/jquery.js"></script>
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.2/moment.min.js"></script>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.6.1/fullcalendar.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        
        <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>
        <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/css/gijgo.min.css" rel="stylesheet" type="text/css" />
        <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">-->
        
        <?php 
            session_start();
            if ($_SESSION['tipo'] < 1){
                echo '<script>location.href = "./index.php";</script>';
            } else if ($_SESSION['tipo'] == 1){
        ?>
            <script src="js/calendarSupervisor.js"></script>  
            <script>
                //VALIDACION DE MODALS PARA EL BOTON
                function valida_envia(){  
                    $('#newEvent').modal('show');
                }       
            </script>
            
        <?php }else {?>
            <script src="js/calendarCostumer.js"></script>            
            <script>
                //VALIDACION DE MODALS PARA EL BOTON
                function valida_envia(){
                    var vToday = new Date().getDay();
                    var h = new Date().getHours();
                    if (vToday > 0 && vToday < 6 && h < 17){                    
                        $('#newEvent').modal('show');
                    }else {
                        $('#warningHora').modal('show');
                    }
                }
                
                function modificarUser(){                   
                    $('#dataUpdateUser').modal('show');
                }
            </script>        
        <?php } ?>        
            <script>
                function salir() {
                    location.href="./extras/exit.php";
                }
            </script>                
        <?php   
            
            include './extras/mUUserUser.php';
            include './eventos/mIEvento.php';
            include './eventos/mAviso.php';
            include './extras/avisoHora.php';
            include './extras/mFinSemana.php';
            include './eventos/mUEvento.php';
            include './eventos/mCEvento.php'; //consulta de evento
        ?>
        
        <a align = center id = "headerFixedPrincipal" class = "contenedor">   
            <div class='fila0'>                
            </div>             
            <h3 class="tituloPareto" > SHIPPING MONITOR 2.0 </h3>        
            <div class="fila1">
                
                <?php if($_SESSION["tipo"] == 1) { ?>
                    <button data-target="#panelAdmin" type="button" class="btn btn-primary btn-sm pull-right glyphicon glyphicon-user" style="float: right; position: absolute; top: 4%; left: 2%; width: 9.6%;" onclick = "location='./adminUsuarios/adminUser.php'"
                        onmouseover="this.style.background='#0B86D4', this.style.cursor='pointer'" onmouseout="this.style.background='#02538B'"> Administracion </button>
                <?php } else {
                ?> 
                <button data-target="#panelAdmin" type="button" class="btn btn-primary btn-sm pull-right glyphicon glyphicon-user" style="float: right; position: absolute; top: 4%; left: 2%; width: 9.6%;" onclick="modificarUser()"
                        onmouseover="this.style.background='#0B86D4', this.style.cursor='pointer'" onmouseout="this.style.background='#02538B'"> Admin Usuario </button>
               
                <?php  } ?>
                <button type="button"class="btn btn-danger btn-sm pull-right glyphicon glyphicon-log-in" onclick="salir()"
                        style="float: left; position: absolute; top: 8%; left: 89%; width: 9.0%;"> Salir</button>
            </div>                
        </a> 
        
    </head>
    <body>
        <div >
            <br><br>
            <!--SEPARADOR PARA DATOS DE INICIO DE SESION-->
            <div>
                <ul class="nav">
                    <li class="nav-item">                 
                        <h5 style="float: right; position: absolute; margin: 1.2% 1%;">Usuario: </h5>                 
                        <input style="float: right; position: absolute; margin: .8% 6%"  id="usuario" value="<?php echo $_SESSION['user']?>" readonly="true"/>
                    </li>
                </ul>   
                <!--BOTON DE AGREGAR EVENTO-->
                <button type="button" class="btn btn-sm btn-warning btnAddUser" onclick="valida_envia()" style="float: right; position: absolute; top: 9%; left: 90%;"><i class='glyphicon glyphicon-plus'></i> Agregar</button>
                <!--<input type="button" value="Enviar" ></td>-->
            </div>
            <br><br><br>
            <div id="calendar" ></div>  
            <div>
                
            </div>
        </div>        
    </body> 
</html>