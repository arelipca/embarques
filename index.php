<?php
session_start();
if (isset($_SESSION['user'])){
    echo '<script>location.href = "./shippE.php";</script>';
} else {

?>
<html>
    <head>
        <title>SHIPPING MONITOR</title>
        <link rel="stylesheet" href="css/fullCalendar.css"/>
        <link rel="stylesheet" href="css/fullcalendar.min.css"/>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
        <script src="https://code.jquery.com/jquery.js"></script>
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.2/moment.min.js"></script>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.6.1/fullcalendar.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>     
        <script src="js/valLogin.js">  </script>

        <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>
        <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/css/gijgo.min.css" rel="stylesheet" type="text/css" />
         
        <?php 
            include './extras/mLogin.php'; 
            include './eventos/mCEvento.php';
        ?>
                
        <a align=center id="headerFixedPrincipal" class="contenedor">   
            <div class='fila0'>                
            </div>             
            <h3 class="tituloPareto" > INICIO </h3>        
            <div class="fila1">
                <button type="button" data-toggle="modal" data-target="#login" class="btn btn-success btn-sm pull-right glyphicon glyphicon-log-in" style="float: left; position: absolute; top: 7%; left: 89%; width: 9.0%;" 
                        onmouseover="this.style.background='#2ECC71', this.style.cursor='pointer'" onmouseout="this.style.background='#008C5B'"> Iniciar Sesion</button>
            </div>                
        </a> 
        
    </head>
    <body>
        <div id="contCalendar" name="contCalendar" onload="setInterval('location.reload()',3000)"> >
            <br><br> <br>
            <div id="calendar" name="calendar" >
                
            </div>  
        </div>        
    </body> 
</html>
<?php } ?>