<?php
    $serverName = "SGLERSQL01\sqlexpress, 1433"; 
    $options = array("Database"=>"DB_LER_SHIPMON_SQL", "UID"=>"USR_SHIPMON_SQL ", "PWD"=>"7ET73jsQxB4hBBrX");
    $conn = sqlsrv_connect($serverName, $options);

    if( $conn === false ) {
        die( print_r( sqlsrv_errors(), true));
    }

    $sql = "SELECT * FROM usuarios ORDER BY usuario";
    $stmt = sqlsrv_query( $conn, $sql );
    if( $stmt === false) {
        die( print_r( sqlsrv_errors(), true) );
    }
?>
<table class="table  table-bordered table-hover">
    <thead>
        <tr>
            <th>Usuario</th>
            <th>Nombre</th>
            <th>Telefono</th>
            <th>Tipo</th>
            <th>Acciones</th>
        </tr>
    </thead>
        <tbody>
            <?php
                while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){               
            ?>
                <tr>
                    <td><?php echo $row['usuario']?></td>
                    <td><?php echo $row['nombre']?></td>
                    <td><?php echo $row['telefono']?></td>
                    <?php 
                        switch ($row['tipo']){
                            case 0:                                    
                                $rowt = "Desactivado";
                            break;
                            case 1:                                    
                                $rowt = "Costumer Services";
                            break;
                            case 2:                                    
                                $rowt = "Almacen";
                            break;
                            case 3:                                    
                                $rowt= "Cambio de estado";
                            break;
                            default :
                            $rowt = 'Error < adminUsuarios class: adminUser line 89 >';
                            break;
                        }
                    ?>
                    
                    <td><?php echo $rowt ?></td>
                    <td>
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#dataUpdate" data-id="<?php echo $row['id']?>" data-usuario="<?php echo $row['usuario']?>" data-nombre="<?php echo $row['nombre']?>" data-telefono="<?php echo $row['telefono']?>" data-tipo="<?php echo $row['tipo']?>" data-contrasena="<?php echo $row['contrasena']?>"  > <i class='glyphicon glyphicon-edit'></i> Modificar</button>
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#dataDelete" data-id="<?php echo $row['id']?>"  ><i class='glyphicon glyphicon-trash'></i> Eliminar</button>
                    </td>
                </tr>
            <?php
            }
            ?>
        </tbody>
</table>