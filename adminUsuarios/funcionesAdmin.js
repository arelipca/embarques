function load(){
    location.href="adminUser.php";
    location.reload(true);
}

////MODIFICACION DE DATOS 
//OBTIENES LOS VALORES DE ACUERDO AL RENGLON Y LOS IMPRIME EN EL MODAL
$('#dataUpdate').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Botón que activó el modal    
    var usuario = button.data('usuario') // Extraer la información de atributos de datos     
    var nombre = button.data('nombre') // Extraer la información de atributos de datos
    var telefono = button.data('telefono') // Extraer la información de atributos de datos
    var tipo = button.data('tipo') // Extraer la información de atributos de datos
    var contrasena = button.data('contrasena') // Extraer la información de atributos de datos
    var estado = button.data('estado') // Extraer la información de atributos de datos

    var modal = $(this)
    modal.find('.modal-title').text('Modificar usuario: ('+usuario+') '+ nombre)
    modal.find('.modal-body #usuario').val(usuario)
    modal.find('.modal-body #nombre').val(nombre)
    modal.find('.modal-body #telefono').val(telefono)
    modal.find('.modal-body #tipo').val(tipo)
    modal.find('.modal-body #contrasena').val(contrasena)
    modal.find('.modal-body #contrasena2').val(contrasena)
    modal.find('.modal-body #estado').val(contrasena)
    $('.alert').hide();//Oculto alert
})

//ACTUALIZA LOS DATOS EN LA BD SE MANDA EL QUERY UPDATE
$( "#actualidarDatos" ).submit(function( event ) {    
    var parametros = $(this).serialize();
    $.ajax({
            type: "POST",
            url: "uUser.php",
            data: parametros,
            beforeSend: function(objeto){
                $("#datos_ajax").html("Mensaje: Cargando...");
            },
            success: function(datos){
                $("#datos_ajax").html(datos);
                load();
            }
    });
    event.preventDefault();
});		

$('#dataDelete').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Botón que activó el modal
    var usuario = button.data('usuario') // Extraer la información de atributos de datos
    var nombre = button.data('nombre')
    
    var modal = $(this)
    modal.find('#user').val(usuario)
    modal.find('.modal-title').text('Eliminar')
    modal.find('.modal-body #txt').text("Estas seguro que quieres eliminar \n Usuario: '"+usuario+"' que pertenece a '"+nombre+"'")
    modal.find('.modal-body #usuario').text(usuario)
    modal.find('.modal-body #nombre').text(nombre)
})

$( "#guardarDatos" ).submit(function( event ) {    
    var parametros = $(this).serialize();
        $.ajax({
                type: "POST",
                url: "../adminUsuarios/iUser.php",
                data: parametros,
                success: function(datos){
                    $("#datos_ajax_register").html(datos);					
                    load();
                }
        });
    event.preventDefault();
});   

$( "#eliminarDatos" ).submit(function( event ) {
    var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "../adminUsuarios/dUser.php",
            data: parametros,
            beforeSend: function(objeto){
                $(".datos_ajax_delete").html("Mensaje: Cargando...");
            },
            success: function(datos){
                $(".datos_ajax_delete").html(datos);
                $('#dataDelete').modal('hide');
                load();
            }
        });
    event.preventDefault();
});


function permite(elEvento, permitidos) {
    // Variables que definen los caracteres permitidos
    var numeros = "0123456789";
    var caracteres = " abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
    var numeros_caracteres = numeros + caracteres;
    var teclas_especiales = [8, 37, 39, 46];
    // 8 = BackSpace, 46 = Supr, 37 = flecha izquierda, 39 = flecha derecha

    // Seleccionar los caracteres a partir del parámetro de la función
    switch(permitidos) {
      case 'num':
        permitidos = numeros;
        break;
      case 'car':
        permitidos = caracteres;
        break;
      case 'num_car':
        permitidos = numeros_caracteres;
        break;
    }

    // Obtener la tecla pulsada 
    var evento = elEvento || window.event;
    var codigoCaracter = evento.charCode || evento.keyCode;
    var caracter = String.fromCharCode(codigoCaracter);

    // Comprobar si la tecla pulsada es alguna de las teclas especiales
    // (teclas de borrado y flechas horizontales)
    var tecla_especial = false;
    for(var i in teclas_especiales) {
        if(codigoCaracter == teclas_especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

    // Comprobar si la tecla pulsada se encuentra en los caracteres permitidos
    // o si es una tecla especial
    return permitidos.indexOf(caracter) != -1 || tecla_especial;
}

    