<head>
    <?php         
    //session_start();
        //CONFIGURAMOS PARA QUE LA HORA SEA LA INTERNACION MEXICANA Y NO LA DE ALEMANIA
        date_default_timezone_set("America/Mexico_City");
        //OBTENEMOS LA FECHA POR SEPARADO
        $d = date("d");
        $ms = date("m");
        $y = date("Y");
        //SOLICITAMOS EL ULTIMO DIA DEL MES PRESENTE        
        $ultimoDiaMes = date("t",mktime(0,0,0,$ms,1,$y));
        
        //FECHA QUE SE ASIGNA COMO MINIMA Y MAXIMA EN EL DATEPICKER
        $fInicio = date('m/d/Y', strtotime('+1 day')) ; 
        $fLimite = date('m/d/Y', strtotime('+7 day')) ; 
        
        // OBTENEMOS LAS HORA ACTUAL, PROBABLEMENTE ESTO NO SIRVA Y SE PASE A 
        // PICKERS HOUR
        $h = explode(";", date("H ;"));
        $m = explode(";", date("i ;"));
        
        $hora = $h[0];      
        $min = $m[0]; 

        //AQUI SE OBTIENEN TODAS LAS HORAS POR RANGOS DE 15 MIN
        for ($i = 0; $i < 24; $i++){    
            if ($i < 10){
                $i = '0'.$i;
            }
            if ($i == 0){
                $x = 0;
            } else {
                $x = $i * 4;
            } 
            for ($j = 0; $j < 60; $j+=15 ){
                if ($j < 10){
                    $horas[$x] = $i.':0'.$j;
                }else{
                    $horas[$x] = $i.':'.$j;
                }
                $x+=1;
            }
        }

        //SE EVALUA DE ACUERDO A LA HORA REAL, PARA NO MOSTRAR HORAS ANTES
        if ($min > 0 && $min < 15 ){
            $val = .25;
        } else if ($min >= 15 && $min < 30){
            $val = .50;
        }else if ($min >= 30 && $min < 45 ){
            $val = .75;
        } else {
            $val = 1.00;
        }

        $ht = ($hora+$val)*4;
        $ht2 = $ht+1;
        
        
        //CONEXION A BD
        $server = "SGLERSQL01\sqlexpress, 1433";  
        $database = "DB_LER_SHIPMON_SQL";  
        $conn = new PDO( "sqlsrv:server=$server ; Database = $database", "USR_SHIPMON_SQL", "7ET73jsQxB4hBBrX");

    ?>
    <script type="text/javascript">
                
        $( function() {
            $("#datepicker").datepicker({ 
                minDate: <?php echo "'$fInicio'" ?>,
                maxDate: <?php echo "'$fLimite'" ?>
            });
        }); 

        $(function() {
            $("#datepicker").datepicker({ 
                onSelect: function(date) { 
                  $fInicio = date;
                  console.log($fInicio);
                  alert('Inicio: '+$fInicio);
                }
            });
        });

        //FUNCION PARA CUANDO SE HACE EL CAMBIO DE SOLD TO PARTY
        //AQUI SE LLENA EL INPUT DEL NOMBRE DEL CLIENTE (SOLD TO PARTY)
        function cambioOpciones(){   
            var id = document.getElementById('soldTo').value;            
            var dataString = 'action='+ id;
            console.log('entro2');

            $.ajax({
                url: './db/getClienteName.php',
                data: dataString,
                cache: false,
                success: function(response){
                    $("#showId").html(response);
                } 
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                        
        }
        
        function permite(elEvento, permitidos) {
    // Variables que definen los caracteres permitidos
            var numeros = "0123456789";
            var caracteres = " abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
            var numeros_caracteres = numeros + caracteres;
            var teclas_especiales = [8, 37, 39, 46];
            // 8 = BackSpace, 46 = Supr, 37 = flecha izquierda, 39 = flecha derecha
            // Seleccionar los caracteres a partir del parámetro de la función
            switch(permitidos) {
              case 'num':
                permitidos = numeros;
                break;
              case 'car':
                permitidos = caracteres;
                break;
              case 'num_car':
                permitidos = numeros_caracteres;
                break;
            }

            // Obtener la tecla pulsada 
            var evento = elEvento || window.event;
            var codigoCaracter = evento.charCode || evento.keyCode;
            var caracter = String.fromCharCode(codigoCaracter);

            // Comprobar si la tecla pulsada es alguna de las teclas especiales
            // (teclas de borrado y flechas horizontales)
            var tecla_especial = false;
            for(var i in teclas_especiales) {
                if(codigoCaracter == teclas_especiales[i]) {
                    tecla_especial = true;
                    break;
                }
            }
            return permitidos.indexOf(caracter) != -1 || tecla_especial;
        }
        
        $( function() {
            $( "#datepicker" ).datepicker();
        } );
    </script>
</head>


<?php 
    if ($_SESSION['tipo'] == 1){ 
?>
    <!--MODAL PARA SUPERVISOR-->
    <form id="modificarEvento" method="post">
        <div id="updateEvent" class="modal fade" tabindex="-1" role="dialog">       
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</b></span></button>
                        <h4 class="modal-title">Edición</h4>
                    </div>
                    <div class="modal-body"> 
                        <div id="datos_ajax"></div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Datos de Evento</h3>
                            </div>
                            <div class="panel-body"> 
                                <div class="row">
                                    <div class="col-md-3" >
                                        <span name="dia" align="center" ><b> Día: </b></span>
                                        <input id="fecha" name="fecha" type="text" style="width: 70%;"  value="<?php echo "$fInicio" ?>" >
                                    </div>
                                    <div class="col-md-5" >
                                        <span name="fecha" align="center" > <b> Hora: </b></span>                                        
                                        <select name="inicio" id="inicio" style="width: 36%;" >
                                            <?php 
                                                for ($x = 0; $x < 96; $x++){ 
                                                    echo "<option value='".$horas[$x]."'>" . $horas[$x]. "</option>";                                       
                                                } 
                                            ?>                                    
                                        </select>
                                        <span><b> a </b></span>
                                        <select name="fin" id="fin"  style=" width: 36%;">
                                            <?php 
                                                for ($x = 0; $x < 96; $x++){ 
                                                    echo "<option value='".$horas[$x]."'>" . $horas[$x]. "</option>";                                         
                                                } 
                                            ?>                                    
                                        </select>
                                        
                                    </div>
                                    <div class="col-md-4" >
                                        <span name="us" align="center" ><b>Usuario: </b></span>
                                        <select id="dUsuario" name="dUsuario" required>
                                            <option value='0' selected > Usuario </option>
                                            <?PHP 
                                                if ( $conn ){
                                                    $stmt = $conn->query( "SELECT usuario FROM usuarios" );  
                                                    $result = sqlsrv_query($conn,$stmt);  

                                                    while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                                                        extract($row);
                                                        echo "<option value='".$usuario."'>" . $usuario. "</option>";
                                                    }
                                                    $result = sqlsrv_close($conn);
                                                } else {
                                                    echo "<option value='0' selected> ERROR: 218 mIEvento </option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>   
                        </div>
                        <div class="panel panel-default" style="margin-top: -10px">
                            <div class="panel-heading">
                                <h3 class="panel-title">Datos de Envio</h3>
                            </div>
                            <div class="panel-body">      
                                <div class="row">
                                    <div class="col-md-4" >
                                        <span><b>Sold to  : </b></span>
                                        <input name="sold" id="sold" style="width: 55%;" readonly>
                                    </div>
                                    <div class="col-md-8" >
                                        <input style="width: 95%" name="nombreCliente" id="nombreCliente" readonly />
                                    </div>
                                </div> 
                                
                                <div class="row" style="margin-top: 5px">
                                    <div class="col-md-4" >
                                        <span><b> Delivery: </b></span> 
                                        <input id="delivery" name="delivery" maxlength="10" style="width: 55%;" placeholder="Num. Delivery" type="text" onkeypress="return permite(event,'num')" required />
                                    </div>
                                    <div class="col-md-4" >
                                        <span><b> Pallets: </b></span>
                                        <input id="carga" name="pallets" maxlength="3" id="pallets" style="width: 50%;" placeholder="Num. Pallets" required />
                                    </div>
                                    <div class="col-md-4">
                                        <span><b> Piezas: </b></span>
                                        <input id="pzas" name="cantidad" maxlength="4" id="cantidad" style="width: 50%;" placeholder="Cantidad Piezas" required />
                                    </div>
                                </div> 
                            </div> 
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">Datos de Transporte</h4>
                            </div>
                            <div class="panel-body"> 
                                
                                <div class="row">
                                    <div class="col-md-4">
                                        <span><b>Placa: </b></span>
                                        <input id="placa" name="placa" style="width: 68%;" maxlength="12" placeholder="Placa" type="text" required />
                                    </div>
                                    <div class="col-md-5" >
                                        <span><b>Ruta: </b></span>
                                        <input id="ruta" name="ruta" style="width: 78%;" maxlength="50" placeholder="Ruta" type="text" required/>
                                    </div>
                                    <div class="col-md-3" >
                                        <select id="dock" name="dock" style="width: 90%; " required>
                                            <option value=""selected="true" disabled="disabled" >Dock</option>
                                            <option value="1" >Dock 1</option>
                                            <option value="2" >Dock 2</option>
                                            <option value="3" >Dock 3</option>
                                            <option value="4" >Dock 4</option>
                                        </select>
                                    </div>
                                </div> 
                                
                                <div class="row" style="margin-top: 5px">
                                    <div class="col-md-12" >
                                        <span><b> Chofer: </b></span> 
                                        <input id="chofer" name="chofer" style="width: 80%;" maxlength="50" placeholder="Chofer" type="text" onkeypress="return permite(event,'car')" required />
                                        <input id="id" name="id" style="width: 5%;" maxlength="50" type="hidden"/>
                                    </div>
                                </div> 
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">Estado</h4>
                            </div>
                            <div class="panel-body">                                 
                                <div class="row">
                                    <div class="col-md-6">
                                        <span><b>Estado: </b></span>
                                        <select id="estado" name="estado" style="width: 60%;" required>
                                            <option value="0" selected="true" disabled="disabled" >Estado</option>
                                            <option value="1"> Shipment Creado </option>
                                            <option value="2"> Loading Start </option>
                                            <option value="3"> Shipment Start </option>
                                            <option value="4"> Shipment Invoiced </option>
                                            <option value="5"> Shipment Complete </option>
                                            <option value="6"> Delay </option>
                                        </select>
                                    </div>
                                    <div class="col-md-6" >
                                        <span><b>N&uacute;m Factura: </b></span>
                                        <input id="factura" name="factura" style="width: 58%;" maxlength="12" placeholder="Número de Factura" type="text" />
                                    </div>
                                </div> 
                            </div>
                        </div>
                        
                        
                    </div><!-- /.modal-content -->
                    <!--                    BOTONES      -->
                    <div class="modal-footer" style="margin-top: -25px">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>                    
                    </div>
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->         
        </div>          
    </form>
    
    <?php } else { ?>

        <!--MODAL PARA COSTUMER SERVICE-->
    <form id="modificarEvento" method="post">
        <div id="updateEvent" class="modal fade" tabindex="-1" role="dialog">       
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</b></span></button>
                        <h4 align="center" class="modal-title">Edición</h4>
                    </div>
                    <div class="modal-body"> 
                        <div id="datos_ajax"></div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Datos de Evento</h3>
                            </div>
                            <div class="panel-body"> 
                                <div class="row">
                                    <div class="col-md-3" >
                                        <span name="dia" align="center" ><b> Día: </b></span>
                                        <input id="fecha" name="fecha" type="text" style="width: 70%;" value="<?php echo "$fInicio" ?>"  readonly>
                                    </div>
                                    <div class="col-md-5" >
                                        <span name="fecha" align="center" > <b> Hora: </b></span>                                        
                                        <input name="inicio" id="inicio" style="width: 20%;" readonly>                                
                                        
                                        <span><b> a </b></span>
                                        <input name="fin" id="fin"  style=" width: 20%;" readonly>
                                        
                                        <input id="id" name="id" style="width: 5%;" maxlength="50" type="hidden"/>
                                    </div>                                    
                                </div>
                            </div>   
                        </div>
                        <div class="panel panel-default" style="margin-top: -10px">
                            <div class="panel-heading">
                                <h3 class="panel-title">Datos de Envio</h3>
                            </div>
                            <div class="panel-body">      
                                <div class="row">
                                    <div class="col-md-4" >
                                        <span><b>Sold to  : </b></span>
                                        <input name="sold" id="sold" style="width: 55%;" readonly>
                                    </div>
                                    <div class="col-md-8" >
                                        <input style="width: 95%" name="nombreCliente" id="nombreCliente" readonly />
                                    </div>
                                </div> 
                                
                                <div class="row" style="margin-top: 5px">
                                    <div class="col-md-4" >
                                        <span><b> Delivery: </b></span> 
                                        <input id="delivery" name="delivery" maxlength="10" style="width: 55%;" placeholder="Num. Delivery" type="text" readonly />
                                    </div>
                                    <div class="col-md-4" >
                                        <span><b> Pallets: </b></span>
                                        <input id="carga" name="pallets" maxlength="3" id="pallets" style="width: 50%;" placeholder="Num. Pallets" readonly />
                                    </div>
                                    <div class="col-md-4">
                                        <span><b> Pizas: </b></span>
                                        <input id="pzas" name="cantidad" maxlength="4" id="cantidad" style="width: 50%;" placeholder="Cantidad Piezas" readonly/>
                                    </div>
                                </div> 
                            </div> 
                        </div>
                       <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">Datos de Transporte</h4>
                            </div>
                            <div class="panel-body"> 
                                
                                <div class="row">
                                    <div class="col-md-4">
                                        <span><b>Placa: </b></span>
                                        <input id="placa" name="placa" style="width: 68%;" maxlength="12" placeholder="Placa" type="text" required />
                                    </div>
                                    <div class="col-md-5" >
                                        <span><b>Ruta: </b></span>
                                        <input id="ruta" name="ruta" style="width: 78%;" maxlength="50" placeholder="Ruta" type="text" required/>
                                    </div>
                                    <div class="col-md-3" >
                                        <select id="dock" name="dock" style="width: 100%; " required>
                                            <option value=""selected="true" disabled="disabled" >Dock</option>
                                            <option value="1" >Dock 1</option>
                                            <option value="2" >Dock 2</option>
                                            <option value="3" >Dock 3</option>
                                            <option value="4" >Dock 4</option>
                                        </select>
                                    </div>
                                </div> 
                                
                                <div class="row" style="margin-top: 5px">
                                    <div class="col-md-12" >
                                        <span><b> Chofer: </b></span> 
                                        <input id="chofer" name="chofer" style="width: 80%;" maxlength="50" placeholder="Chofer" type="text" onkeypress="return permite(event,'car')" required />
                                    </div>
                                </div> 
                            </div>
                       </div>                    
                    </div><!-- /.modal-content -->
                    <!--                    BOTONES      -->
                    <div class="modal-footer" style="margin-top: -25px">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>                    
                    </div>
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->         
        </div>          
    </form>
    <?php } ?>
</body>
    


