<head>
    <?php                
        //CONFIGURAMOS PARA QUE LA HORA SEA LA INTERNACION MEXICANA Y NO LA DE ALEMANIA
        date_default_timezone_set("America/Mexico_City");
        //OBTENEMOS LA FECHA POR SEPARADO
        $d = date("d");
        $ms = date("m");
        $y = date("Y");
        //SOLICITAMOS EL ULTIMO DIA DEL MES PRESENTE        
        $ultimoDiaMes = date("t",mktime(0,0,0,$ms,1,$y));
        
        //FECHA QUE SE ASIGNA COMO MINIMA Y MAXIMA EN EL DATEPICKER
        $fInicio = date('m/d/Y', strtotime('+1 day')) ; 
        $fLimite = date('m/d/Y', strtotime('+7 day')) ; 
        
        // OBTENEMOS LAS HORA ACTUAL, PROBABLEMENTE ESTO NO SIRVA Y SE PASE A 
        // PICKERS HOUR
        $h = explode(";", date("H ;"));
        $m = explode(";", date("i ;"));
        
        $hora = $h[0];      
        $min = $m[0]; 

        //AQUI SE OBTIENEN TODAS LAS HORAS POR RANGOS DE 15 MIN
        for ($i = 0; $i < 24; $i++){    
            if ($i < 10){
                $i = '0'.$i;
            }
            if ($i == 0){
                $x = 0;
            } else {
                $x = $i * 4;
            } 
            for ($j = 0; $j < 60; $j+=15 ){
                if ($j < 10){
                    $horas[$x] = $i.':0'.$j;
                }else{
                    $horas[$x] = $i.':'.$j;
                }
                $x+=1;
            }
        }

        //SE EVALUA DE ACUERDO A LA HORA REAL, PARA NO MOSTRAR HORAS ANTES
        if ($min > 0 && $min < 15 ){
            $val = .25;
        } else if ($min >= 15 && $min < 30){
            $val = .50;
        }else if ($min >= 30 && $min < 45 ){
            $val = .75;
        } else {
            $val = 1.00;
        }

        $ht = ($hora+$val)*4;
        $ht2 = $ht+1;
        
    ?>
    <script type="text/javascript">        
            $( function() {
                $("#datepicker").datepicker({ 
                    minDate: <?php echo "'$fInicio'" ?>,
                    maxDate: <?php echo "'$fLimite'" ?>
                });
            }); 

            $(function() {
                $("#datepicker").datepicker({ 
                    onSelect: function(date) { 
                      $fInicio = date;
                      console.log($fInicio);
                      alert('Inicio: '+$fInicio);
                    }
                });
            });

        //FUNCION PARA CUANDO SE HACE EL CAMBIO DE SOLD TO PARTY
        //AQUI SE LLENA EL INPUT DEL NOMBRE DEL CLIENTE (SOLD TO PARTY)
        function cambioOpciones(){   
            var id = document.getElementById('soldTo').value;            
            var dataString = 'action='+ id;
            console.log('entro2');

            $.ajax({
                url: './db/getClienteName.php',
                data: dataString,
                cache: false,
                success: function(response){
                    $("#showId").html(response);
//                        alert('Termino');
                } 
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                if (jqXHR.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + jqXHR.responseText);
                }
            });                        
        }
        
        function permite(elEvento, permitidos) {
    // Variables que definen los caracteres permitidos
            var numeros = "0123456789";
            var caracteres = " abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
            var numeros_caracteres = numeros + caracteres;
            var teclas_especiales = [8, 37, 39, 46];
            // 8 = BackSpace, 46 = Supr, 37 = flecha izquierda, 39 = flecha derecha
            // Seleccionar los caracteres a partir del parámetro de la función
            switch(permitidos) {
              case 'num':
                permitidos = numeros;
                break;
              case 'car':
                permitidos = caracteres;
                break;
              case 'num_car':
                permitidos = numeros_caracteres;
                break;
            }

            // Obtener la tecla pulsada 
            var evento = elEvento || window.event;
            var codigoCaracter = evento.charCode || evento.keyCode;
            var caracter = String.fromCharCode(codigoCaracter);

            // Comprobar si la tecla pulsada es alguna de las teclas especiales
            // (teclas de borrado y flechas horizontales)
            var tecla_especial = false;
            for(var i in teclas_especiales) {
                if(codigoCaracter == teclas_especiales[i]) {
                    tecla_especial = true;
                    break;
                }
            }

    // Comprobar si la tecla pulsada se encuentra en los caracteres permitidos
    // o si es una tecla especial
            return permitidos.indexOf(caracter) != -1 || tecla_especial;
        }
        
        $( function() {
            $( "#datepicker" ).datepicker();
        } );
    </script>
</head>

<?php  if ($_SESSION["tipo"] == 1 ){ ?>
<body>    
    <form id="modificarEvento" method="post">
        <div id="updateEvent" class="modal fade" tabindex="-1" role="dialog">       
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 align=center class="modal-title">Edición</h4>
                    </div>
                    <div class="modal-body">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Datos de Evento</h3>
                            </div>
                            <div class="panel-body"> 
                                <div style="float:left; width: 28%; padding: 1px; display: block; margin: auto 7px;">
                                    <span style="float:left; width: 5%; padding: 1px; display: block; margin: auto 2px;" name="fecha" align="center" >Día: </span>
                                    <input style="float:left; width: 60%; padding: 1px; display: block; margin: auto 20px;" type="text" id="datepicker" name="datepicker" value="<?php echo "$fInicio" ?>" >
                                </div>

                                <span style="float:left; width: 5%; padding: 1px; display: block; margin: auto 7px;" name="Hora" align="center" >Hora: </span>
                                <select style="float:left; width: 13%; padding: 1px; display: block; margin: auto 7px;" name="start" id="start" >
                                    <?php 
                                        for ($x = 0; $x < 96; $x++){ 
                                            echo "<option value='".$horas[$x]."'>" . $horas[$x]. "</option>";
//                                            echo $horas[$x],'<br>';                                          
                                        } 
                                    ?>                                    
                                </select>
                                
                                <select style="float:left; width: 13%; padding: 1px; display: block; margin: auto 7px;" name="end" id="end" >
                                    <?php 
                                        for ($x = 0; $x < 96; $x++){ 
                                            echo "<option value='".$horas[$x]."'>" . $horas[$x]. "</option>";
//                                            echo $horas[$x],'<br>';                                          
                                        } 
                                    ?>                                    
                                </select>
                                
                                <input id="id" name="id" style="visibility: hidden" >
                                <!--<input style="float:left; width: 10%; padding: 1px; display: block; margin: auto 7px;" name="start" id="start" value=" </input>-->
<!--                                <input style="float:left; width: 10%; padding: 1px; display: block; margin: auto 7px;" name="end" id="end" value="<?php echo $hFin ?>" readonly/>-->
                            </div>   
                        </div>
                        <div class="panel panel-default" style="margin-top: -10px">
                            <div class="panel-heading">
                                <h3 class="panel-title">Datos de Envio</h3>
                            </div>
                            <div class="panel-body">  
                                
                                <select style="float:left; width: 35%; padding: 1.5px; display: block; margin: auto 0px;" id='sold' name="cliente" onchange='cambioOpciones();' required>
                                    <option value='0' selected disabled="disabled">Sold To Party</option>
                                    <?php
                                        $server = "SGLERSQL01\sqlexpress, 1433";  
                                        $database = "DB_LER_SHIPMON_SQL";  
                                        $conn = new PDO( "sqlsrv:server=$server ; Database = $database", "USR_SHIPMON_SQL", "7ET73jsQxB4hBBrX");

                                        if ( $conn ){
                                            $stmt = $conn->query( "SELECT * FROM clientes" );  
                                            $result = sqlsrv_query($conn,$stmt);  

                                            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                                                extract($row);
                                                echo "<option value='".$soldToParty."'>" . $soldToParty. "</option>";
                                            }
                                            $result = sqlsrv_close($conn);
                                        } else {
                                            echo "<option value='0' selected> ERROR: 173 mIEvento </option>";
                                        }
                                    ?>
                                </select>
                                
                                <input style="width: 320px;" name="nombreCliente" id="nombreCliente" readonly required/>
                                <!--<div style="float:left; width: 60%; padding: 15px; display: block; margin: auto 7px; border: 1px solid red;" id="showId" >-->
<!--                                <div style="float:left; width: 60%; padding: 3px; display: block; margin: -1px 7px;" id="showId" > 
                                    <input style="width: 320px;" name="nameCliente" id="nameCliente" readonly required/>
                                </div>                                -->
                                <!--<input name="nameCliente" id="nameCliente" >-->
                                
                                <!--<input style="float:left; width: 60%; padding: 1px; display: block; margin: auto 7px;" name="nameCliente" id="nameCliente" value="Prueba" readonly/>-->
                                <div class="row">
                                    <input id="delivery" name="delivery" maxlength="10" style="float:left; width: 33%; padding: 1px; display: block; margin: 5px 17px;" placeholder="Num. Delivery" type="text" onkeypress="return permite(event,'num')" readonly required />
                                    <input id="carga" name="carga" maxlength="3" style="float:left; width: 28%; padding: 1px; display: block; margin: 5px -9px;" placeholder="Num. Pallets" type="text" onkeypress="return permite(event,'num')" readonly required />
                                    <input id="pzas" name="pzas" maxlength="4" style="float:left; width: 28%; padding: 1px; display: block; margin: 5px 15px;" placeholder="Cantidad Piezas" type="text"  onkeypress="return permite(event, 'num')" readonly required />
                                </div>
                            </div> 
                        </div>
                       <div class="panel panel-default" style="margin-top: -10px">
                            <div class="panel-heading">
                                <h4 class="panel-title">Datos de Transporte</h4>
                            </div>
                            <div class="panel-body">  
                                <input id="placa" name="placa" style="float:left; width: 24%; padding: 1px; display: block; margin: auto auto;" maxlength="12" placeholder="Placa" type="text" required />
                                <input id="ruta" name="ruta" style="float:left; width: 55%; padding: 1px; display: block; margin: auto 5px;" placeholder="Ruta" type="text" required/>
                                <select id="dock" name="dock" style="float:left; width: 14%; padding: 3px; display: block; margin: auto 2px;" required>
                                    <option value="0"selected="true" disabled="disabled" >Dock</option>
                                    <option value="1" >Dock 1</option>
                                    <option value="2" >Dock 2</option>
                                </select>
                                <div class="row">                                    
                                    <input id="chofer" name="chofer" style="float:left; width: 90%; padding: 1px; display: block; margin: 5px 15px;" placeholder="Chofer" type="text" onkeypress="return permite(event,'car')" required />
                                </div>
                                
                                <select id="estado" name="estado" style="float:left; width: 30%; padding: 3px; display: block; margin: auto 2px;" required>
                                    <option value="0" selected="true" disabled="disabled" >Estado</option>
                                    <option value="1"> Shipment Creado </option>
                                    <option value="2"> Loading Start </option>
                                    <option value="3"> Shipment Start </option>
                                    <option value="4"> Shipment Invoiced </option>
                                    <option value="5"> Shipment Complete </option>
                                    <option value="6"> Delay </option>
                                </select>
                                
                            </div>
                       </div>                    
                    </div><!-- /.modal-content -->
                    <!--                    BOTONES      -->
                    <div class="modal-footer" style="margin-top: -25px">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>                    
                    </div>
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->         
        </div>          
    </form>
<?php } else {?>
    
    
    <?php }?>
</body>
    


