<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *         CREADO POR : ARELI P. CALIXTO
 *  FECHA DE CREACION : 20/06/2018
 *        DESCRIPCION : MODAL SOLO PARA VISUALIZACION DE EVENTO 
 */
?>

<html>
    <head></head>
    <body>    
        <form id="verEvento" method="post">
            <div id="selectEvent" class="modal fade" tabindex="-1" role="dialog">       
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 align=center class="modal-title">Visualizar</h4>
                        </div>
                         <div class="modal-body"> 
                            <div id="datos_ajaxCons"></div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Datos de Evento</h3>
                                </div>
                                <div class="panel-body"> 
                                    <div class="row">
                                        <div class="col-md-3" >
                                            <span name="diaCons" align="center" ><b> Día: </b></span>
                                            <input id="fechaCons" name="fechaCons" type="text" style="width: 70%;" value="<?php echo "$fInicio" ?>"  readonly>
                                        </div>
                                        <div class="col-md-4" >
                                            <span name="fechaCons" align="center" > <b> Hora: </b></span>                                        
                                            <input name="inicioCons" id="inicioCons" style="width: 25%;" readonly>                                

                                            <span><b> a </b></span>
                                            <input name="finCons" id="finCons"  style=" width: 25%;" readonly>
                                        </div> 
                                        <div class="col-md-5" >
                                            <span name="fechaCons" align="center" ><b>Estado: </b></span>                                        
                                            <input name="estadoCons" id="estadoCons" style="width: 70%;" readonly>  
                                        </div> 
                                    </div>
                                </div>   
                            </div>
                            <div class="panel panel-default" style="margin-top: -10px">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Datos de Envio</h3>
                                </div>
                                <div class="panel-body">      
                                    <div class="row">
                                        <div class="col-md-4" >
                                            <span><b>Sold to  : </b></span>
                                            <input name="soldCons" id="soldCons" style="width: 55%;" readonly>
                                        </div>
                                        <div class="col-md-8" >
                                            <input style="width: 95%" name="nombreClienteCons" id="nombreClienteCons" readonly />
                                        </div>
                                    </div> 

                                    <div class="row" style="margin-top: 5px">
                                        <div class="col-md-4" >
                                            <span><b> Delivery: </b></span> 
                                            <input id="deliveryCons" name="deliveryCons" maxlength="10" style="width: 55%;" placeholder="Num. Delivery" type="text" readonly />
                                        </div>
                                        <div class="col-md-4" >
                                            <span><b> Pallets: </b></span>
                                            <input id="cargaCons" name="palletsCons" maxlength="3" id="palletsCons" style="width: 50%;" placeholder="Num. Pallets" readonly />
                                        </div>
                                        <div class="col-md-4" >
                                            <span><b> Pizas: </b></span>
                                            <input id="pzasCons" name="cantidadConsCons" maxlength="4" id="cantidadCons" style="width: 50%;" placeholder="CantidadCons Piezas" readonly/>
                                        </div>
                                    </div> 
                                </div> 
                            </div>
                           <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Datos de Transporte</h4>
                                </div>
                                <div class="panel-body"> 

                                    <div class="row">
                                        <div class="col-md-4" >
                                            <span><b>Placa: </b></span>
                                            <input id="placaCons" name="placaCons" style="width: 68%;" maxlength="12" placeholder="Placa" type="text" readonly />
                                        </div>
                                        <div class="col-md-5" >
                                            <span><b>Ruta: </b></span>
                                            <input id="rutaCons" name="rutaCons" style="width: 78%;" maxlength="50" placeholder="Ruta" type="text" readonly />
                                        </div>
                                        <div class="col-md-3" >
                                            <select id="dockCons" name="dockCons" style="width: 100%; " disabled="true" readonly>
                                                <option value="" selected="false"  disabled="true" >Dock</option>
                                                <option value="1" >Dock 1</option>
                                                <option value="2" >Dock 2</option>
                                                <option value="3" >Dock 3</option>
                                                <option value="4" >Dock 4</option>
                                            </select>
                                        </div>
                                    </div> 

                                    <div class="row" style="margin-top: 5px">
                                        <div class="col-md-12" >
                                            <span><b> Chofer: </b></span> 
                                            <input id="choferCons" name="choferCons" style="width: 80%;" maxlength="50" placeholder="Chofer" type="text" onkeypress="return permite(event,'car')" required />
                                        </div>
                                    </div> 
                                </div>
                           </div>                    
                         </div >
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->         
            </div>          
        </form>
    </body>
    
    
</html>

